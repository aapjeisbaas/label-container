# label-container

## How to start
To start the container and automaticly connect the usb dymo you can use the following start/restart script:

```bash
#!/bin/bash

label_container_id=$(docker ps | grep 'aapjeisbaas/label-container' | awk '{print $1}')
if [[ ${#label_container_id} -gt 0  ]]; then
  docker kill $label_container_id
  echo "found label container, killing it before starting a new one."
else
  echo "no running instance found, starting a fresh one."
fi

docker pull aapjeisbaas/label-container
docker run -p 9898:5000 -d --device=/dev/bus/usb/$(lsusb -d 0922:001f | awk '{print $2 "/" $4}' | sed 's/://g') aapjeisbaas/label-container
```
This mounts the usb device with id `0922:001f` to the label-container, if your dymo has a different id in `lsusb` just change it in the script.

## How to use
This container provides a webpage where you can upload pdf labels, if you want to push labels from python you can use tha example below.

```python
import requests

def send_to_printer(pdf):
    files = {'file': open(pdf, 'rb')}
    requests.post('http://127.0.0.1:5000/uploader', files=files)

send_to_printer('/home/user/pdf/print-this.pdf')
```

---

Find usb printer with `lsusb`

```bash
   lsusb
Bus 004 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 004 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 003: ID 046d:c338 Logitech, Inc. 
Bus 002 Device 005: ID 0922:0020 Dymo-CoStar Corp. LabelWriter 450
Bus 002 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 005: ID 0a12:0001 Cambridge Silicon Radio, Ltd Bluetooth Dongle (HCI mode)
Bus 001 Device 004: ID 1a40:0201 Terminus Technology Inc. FE 2.1 7-port Hub
Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

To give the container access to the device start ik with `--device`

```bash
docker run -ti --device=/dev/bus/usb/002/005 label
```

If you succesfully mount the usb device in the container you should be able to find it within the container like this:

`docker exec -t -i c43f26aa88fa bash`

```
root@c43f26aa88fa:/# lpinfo -v
file cups-pdf:/
network ipp14
network beh
network socket
network https
network ipp
network ipps
network lpd
network http
direct usb://DYMO/LabelWriter%20450?serial=01010112345600
root@c43f26aa88fa:/#
```

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Flabel-container%2FREADME.md&dt=label-container)
