FROM python:slim-buster
ADD . /app
RUN pip install -r /app/requirements.txt && \
    apt-get update && apt-get install -y --no-install-recommends \
	lpr \
        cups \
        cups-client \
        cups-pdf \
        printer-driver-dymo \
	netcat \
	wget \
	&& rm -rf /var/lib/apt/lists/* && \
    cd /app && \
    wget http://download.dymo.com/dymo/Software/Download%20Drivers/Linux/Download/dymo-cups-drivers-1.4.0.tar.gz && \
    tar -xzf dymo-cups-drivers-1.4.0.tar.gz && \
    cp dymo-cups-drivers-1.4.0.5/ppd/* /usr/share/cups/model
CMD ["/app/start.sh"]
